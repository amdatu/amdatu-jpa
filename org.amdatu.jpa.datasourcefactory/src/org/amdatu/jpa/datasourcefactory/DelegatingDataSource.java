/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.datasourcefactory;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import javax.sql.DataSource;
import javax.transaction.TransactionManager;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.managed.BasicManagedDataSource;
import org.osgi.service.jdbc.DataSourceFactory;
import org.osgi.service.log.LogService;

/**
 * DelegatingDataSource adds additional service dependencies based on DataSource configuration
 * and creates a DataSource instance when all service dependencies are available.
 * 
 * All DataSource operations are delegated to a {@link BasicDataSource} (managed=false)
 * or {@link BasicManagedDataSource} (managed=true) depending on DataSource configuration
 *
 */
public class DelegatingDataSource implements DataSource {

    private volatile LogService m_logService;

    private volatile DataSourceFactory m_dataSourceFactory;

    private volatile TransactionManager m_transactionManager;

    private volatile BasicDataSource m_dataSource;

    private DataSourceBuilder m_builder;
    
    public DelegatingDataSource(DataSourceBuilder builder) {
        m_builder = builder;
    }
    
    @SuppressWarnings("unused")
    private void start() {
        try {
            m_dataSource = m_builder.build(m_dataSourceFactory.createDriver(null), m_transactionManager);
        }
        catch (SQLException e) {
            m_logService.log(LogService.LOG_ERROR, "Failed to create DataSource " + e.getMessage(), e);
        }
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void stop() {
        if (m_dataSource != null) {
            try {
                m_dataSource.close();
            }
            catch (SQLException e) {
                m_logService.log(LogService.LOG_ERROR, "Failed to close DataSource " + e.getMessage(), e);
            }
        }
        m_dataSource = null;
    }

    public PrintWriter getLogWriter() throws SQLException {
        return m_dataSource.getLogWriter();
    }

    public <T> T unwrap(Class<T> iface) throws SQLException {
        return m_dataSource.unwrap(iface);
    }

    public void setLogWriter(PrintWriter out) throws SQLException {
        m_dataSource.setLogWriter(out);
    }

    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return m_dataSource.isWrapperFor(iface);
    }

    public Connection getConnection() throws SQLException {
        return m_dataSource.getConnection();
    }

    public void setLoginTimeout(int seconds) throws SQLException {
        m_dataSource.setLoginTimeout(seconds);
    }

    public Connection getConnection(String username, String password) throws SQLException {
        return m_dataSource.getConnection(username, password);
    }

    public int getLoginTimeout() throws SQLException {
        return m_dataSource.getLoginTimeout();
    }

    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return m_dataSource.getParentLogger();
    }

}